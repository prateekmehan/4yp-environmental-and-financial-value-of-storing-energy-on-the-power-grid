%% Title

% Prateek's 4YP - linear optimiser with roundtrip efficiency
% 22nd February 2021

% This optimiser factors in the marginal carbon emissions using a carbon
% price. See report for more details on the code.

clc
clear

%% Import data

tic

% Price data (GBP/MWh)
price_data = xlsread('DAM_test_25_samples.xlsx');

% Marginal carbon emissions data (gCO2eq/MWh)
marginal_emissions = xlsread('Emissions_test_25_samples.xlsx').*1000;

% Check that data are the same size
if size(price_data) ~= size(marginal_emissions)
    disp('Error: Different data sizes.')
    return
end

% Number of samples in both data sets
N = size(price_data,1);
fprintf('Linear optimiser running with %d samples. \n',N)

M = N-1; % to tidy up code below

%% User inputs

% Time step (hours)
dt = 1;

% Pmax of battery (MW)
Pmax = 10;

% Emin and Emax of battery (MWh)
Emin = 0;
Emax = 10;

% Capex of battery (GBP/kWh)
capex = 200;

% Lifetime of battery (cycles)
lifetime = 5000;

% Roundtrip efficiency of battery
eta = 0.85;

% Carbon price (GBP/tonnesCO2eq)
lambda_input = 0;

%% Initial calculations from user inputs

% Cost of degradation (GBP/MWh)
cost_of_Elost = capex*1000;

% Carbon price calculation (GBP/gCO2eq)
lambda = lambda_input/(1e6);

% Integral constant
D = 1/(2*5*lifetime);

%% Setting the objective function, f

% f = [charge cost, discharge revenue, power, absolute powers and SoC, cost_of_Elost]
f_1 = transpose(price_data + lambda.*marginal_emissions);
f = [zeros(1,2*N), f_1, zeros(1,2*N), cost_of_Elost];

%% Setting the lower and upper bounds, LB and UB

LB_1 = zeros(1,2*N);            % Initial charge and discharge power must be positive
LB_2 = [0];                     % Inital power must be positive as there is no initial charge
LB_3 = -Pmax.*ones(1,M);        % Representing full discharge
LB_4 = zeros(1,N);              % Absolute power cannot be negative
LB_5 = Emin.*ones(1,N);         % Absolute power and SoC
LB_6 = [-inf];                  % Elost

LB = [LB_1, LB_2, LB_3, LB_4, LB_5, LB_6];

UB_1 = Pmax.*ones(1,N);         % Pmax charging power
UB_2 = [0];                     % Initial discharge power is zero
UB_3 = Pmax.*ones(1,M);         % Pmax discharging power
UB_4 = Pmax.*ones(1,N);         %
UB_5 = inf.*ones(1,N);          %
UB_6 = [0];                     % Initial SoC is zero
UB_7 = Emax.*ones(1,M);         % Emax SoC
UB_8 = [inf];                   %

UB = [UB_1, UB_2, UB_3, UB_4, UB_5, UB_6, UB_7, UB_8];

%% Setting inequality constraints, A and b

% split A into 6 parts
A_1 = zeros(N,2*N);
A_2 = eye(N);
A_3 = -1.*eye(N);
A_4 = zeros(N,N+1);

A = [A_1, A_2, A_3, A_4; A_1, A_3, A_3, A_4];

b = zeros(2*N,1);

%% Setting equality constraints, Aeq and beq

Aeq_1 = eye(N);
Aeq_2 = -1.*eye(N);
Aeq_3 = zeros(N,(2*N+1));
Aeq_4 = eta*dt.*eye(N);
Aeq_5 = -dt.*eye(N);
Aeq_6 = zeros(N,2*N);
Aeq_7 = diag(ones(1,N-1),-1) + -1.*eye(N);
Aeq_8 = zeros(N,1);                                             % last column
Aeq_9 = [zeros(1,3*N), D.*dt.*ones(1,N), zeros(1,N), -1];       % bottom row

Aeq = [Aeq_1, Aeq_2, Aeq_2, Aeq_3; Aeq_4, Aeq_5, Aeq_6, Aeq_7, Aeq_8; Aeq_9];

beq = zeros(2*N+1,1);

%% Run linear programming function

x = linprog(f,A,b,Aeq,beq,LB,UB);

%% Plots

I = 1:1:N;

price = transpose(price_data);
emissions = transpose(marginal_emissions)./(1e6);   % emissisions in tonnes
power = x((2*N+1):1:(3*N));
charge = x(1:1:N);
discharge = x((N+1):1:(2*N));
energy = x(((N*4)+1):1:(end-1));

figure;
subplot(3,1,1);
plot(I,price,'*','Color',[1 0.6 0.6],'LineWidth',3); ylabel('Price (GBP/MWh)'); grid ON;
xlabel('Hours')

a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',14)

%subplot(3,1,1);
%plot(I,emissions,'*','Color',[0.6 1 0.6],'LineWidth',3); ylabel('MEFs (tCO2/MWh)'); grid ON;
%xlabel('Hours')

%a = get(gca,'XTickLabel');
%set(gca,'XTickLabel',a,'fontsize',14)

subplot(3,1,2);
plot(I,power,'x','Color',[0.6 0.8 1],'LineWidth',3); ylabel('Start Power (MW)'); grid ON; 
xlabel('Hours')

a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',14)

%subplot(6,1,4);
%plot(I,charge,'bx'); ylabel('Start Charging Power (MW)'); grid ON; 

%subplot(6,1,5);
%plot(I,discharge,'bx'); ylabel('Start Discharging Power (MW)'); grid ON; 

subplot(3,1,3);
plot(I,energy,'Color',[1 0.8 0.6],'LineWidth',3); ylabel('End Energy (MWh)'); grid ON; 
xlabel('Hours')

a = get(gca,'XTickLabel');
set(gca,'XTickLabel',a,'fontsize',14)


%% Calculating totals

% Revenue from discharging (GBP)
d_revenue = sum(transpose(x((N+1):1:(2*N))).*price)*dt;
fprintf('Total revenue from discharging is %5.2f GBP. \n',d_revenue)

% Cost from charging (GBP)
c_cost = sum(transpose(x(1:1:N)).*price)*dt;
fprintf('Total cost from charging is %5.2f GBP. \n',c_cost)

% Total profit from arbitrage (GBP)
total_arbitrage_profit = sum(-1.*price.*transpose(power))*dt;
fprintf('Total arbitrage profit is %5.2f GBP. \n',total_arbitrage_profit)

% Total cost of degradation (GBP)
total_cost_of_degradation = x(5*N+1)*f(5*N+1);
fprintf('Total cost of degradation is %5.2f GBP. \n',total_cost_of_degradation)

% Total profit (GBP)
total_profit = total_arbitrage_profit - total_cost_of_degradation;
fprintf('Total profit is %5.2f GBP. \n',total_profit)

% Total marginal carbon emissions (tonnesCO2)
total_marginal_emissions = sum(emissions.*transpose(power))*dt;
fprintf('Total marginal carbon emissions is %f tonnesCO2eq. \n',total_marginal_emissions)

toc

% end of script.

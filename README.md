This is supplementary material for the 4th year project titled "Environmental and Financial Value of Storing Energy on the Power Grid".

Author: Prateek Mehan

Supervisor: Dr David Howey

Engineering Science Department at the University of Oxford.
